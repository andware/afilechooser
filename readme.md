#android文件选择器 
##一、在需要调用的 Activity 中写 onResult 方法 
首先声明
```

	private static final int REQUEST_CODE = 6384; // onActivityResult request code
```
```

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CODE:	
			// If the file selection was successful
			if (resultCode == RESULT_OK) {		
				if (data != null) {
					// Get the URI of the selected file
					final Uri uri = data.getData();

					try {
						// Create a file instance from the URI
						final File file = FileUtils.getFile(uri);
						if ( file.getName().contains(".ecr") ) {
							FileDataDeal.CopyFile(file, myAppPath);
						} else {
							Toast.makeText(getApplicationContext(), "File seect error,Please try again!", Toast.LENGTH_SHORT).show();
						}
					} catch (Exception e) {
						Log.e("FileSelectorTestActivity", "File select error", e);
					}
				}
			} 
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
```
##二、在需要调用的地方如下填写
```
Intent intent = new Intent();
intent.setClass(getApplicationContext(), com.ipaulpro.afilechooser.FileChooserActivity.class);
startActivityForResult(intent, REQUEST_CODE);				

```
##三、在AndroidMainfest 中声明 
```
    	<activity
            android:name="com.ipaulpro.afilechooser.FileChooserActivity"
            android:exported="false"
            android:icon="@drawable/ic_chooser"
            android:label="@string/choose_file"
            android:theme="@style/ChooserTheme" >
            <intent-filter>
                <action android:name="android.intent.action.GET_CONTENT" />

                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.OPENABLE" />

                <data android:mimeType="*/*" />
            </intent-filter>
        </activity>
```
###注：有 aFileChooserExample 的项目示例可供查看